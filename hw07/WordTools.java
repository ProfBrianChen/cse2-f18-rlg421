//hw 07
//rebecca grady
//november 6, 2018

//implement a sampleText() method that prompts the user to enter a string of their choosing
//store the sample text as a string and output that string

//implement a printMenu() method 
//outputs a menu of the user options for editing or analyzing the string
//returns the user's entered menu option
//each option is reprsented by a single character
//if invalid answer is entered keep prompting until correct answer
//hint: implement quit before implementing other options
//call printMenu() in the main() method
//in main menu call printMenu() each time the user enters an incorrect input
//and after each time the user enters an acceptable input until the user enters q to Quit
//call a method for each letter 

//getNumOfNonWSCharacters() method
//has a string as a parameter and returns the number of characters in the String 
//excludes white spaces

//getNumOfWords() method
//getNumOfWords() has a string as a parameter and returns the number of words in the string

//findText() method --> two strings as parameters
//first parameter is the text to be found in the user provided sample text
//second parameter is the user provided sample text

//implement the replaceExclamation() method
//has a string parameter and returns a string which replaces each '!' character in the string with a '.' 

import java.util.Scanner;
import java.lang.Character;

public class WordTools{ //main method required for every java program 
  public static String sampleText(){ //prompts the user to enter a string of their choosing
    Scanner myScanner = new Scanner(System.in);
    String text = myScanner.nextLine(); //store the sample text as a string and output that string
    System.out.println(text);
    return text;
  }
  
  public static void printMenu(String text){ //outputs a menu of the user options for editing or analyzing the string
    String option = ""; //need to define option outside of the do while so you can use it --> has to do with the scope of the variable
    do{
      System.out.println("MENU"); //prints each menu function
      System.out.println("c - Number of non-whitespace characters");
      System.out.println("w - Number of words");
      System.out.println("f - Find text");
      System.out.println("r - Replace all !'s");
      System.out.println("s - Shorten spaces");
      System.out.println("q - Quit");
      
      Scanner myScanner = new Scanner( System.in );
      option = myScanner.next();
      
      //what to do depending on what character a person enters
      if (option.equals("c")){
        System.out.println("The number of characters: " + getNumOfNonWSCharacters(text));
      }
      else if (option.equals("w")){
        System.out.println("The number of words in the text is: " + getNumOfWords(text));
      }
      else if (option.equals("f")){
        myScanner.nextLine();
        System.out.print("Enter a word or phrase to be found: ");
        String find = myScanner.nextLine();
        findText(text, find);
      }
      else if (option.equals("r")){
        System.out.println(replaceExclamation(text));
      }
      else if(option.equals("q")){
        System.out.println();
      }
      else if(option.equals("s")){
        System.out.println(shortenSpace(text));
      }
      else{
        System.out.println("did not enter a correct character please try again");
        myScanner.nextLine(); //clears the incorrect value
      }
    } while(!option.equals("c") && !option.equals("w") && !option.equals("f") && !option.equals("r") && !option.equals("q") && !option.equals("s"));
  } //while it is not c, w, f, r, q, or s you want to redo the loop to get a correct value
  
  
  public static int getNumOfNonWSCharacters(String text){ //has a string as a parameter and returns the number of characters in the String 
    int counter = 0;
    for (int i = 0; i<text.length();i++){
      if (!isWhiteSpace("" + text.charAt(i)) == true){ //since the method accepts a string need to put "" first
        counter = counter + 1;
      }
    }
    return counter;  
  }
  
  public static boolean isWhiteSpace(String a) { //made a method for detecting white spaces because i needed it twice so this was easier
    if (a.equals(" "))
      return true;
    else
      return false;
  }
  
  public static int getNumOfWords(String text){ //has a string as a parameter and returns the number of characters in the String 
    int counter = 0;
    for (int i = 0; i<text.length(); i++){
      if (isWhiteSpace("" + text.charAt(i)) == true){ //since the method accepts a string need to put "" first
        counter = counter + 1;
    } 
  }
    return counter + 1;
  }
  
  public static int findText(String text, String find){ //first parameter is the text to be found in the user provided sample text
//second parameter is the user provided sample text
    int count = 0; //for how many there are
    while (text.length() >= 0)
    {
      if (text.contains(find))
      {
        count ++;
        text = text.substring(text.indexOf(find)); 
      }
      else
        return count; 
      
    }
    return 0; //never happens
  }
  
  public static String replaceExclamation(String text){ //
    char r = '.';//character to replace it
    char e = '!';//character to replace
    String textTwo = text.replace(e, r);
    return textTwo;
  }
  
  public static String shortenSpace(String text){
    String s = " ";
    String d = "  ";
    
    String shortText = text.replace(d, s); //replaces the double space with a single space
    
    return shortText;
  }
  
  public static void main(String [] args){
    printMenu(sampleText()); //have sample text as the input bc that is what each print menu function uses 
  }
}
  
  