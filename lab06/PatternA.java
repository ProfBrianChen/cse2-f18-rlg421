//rebecca grady 
//CSE 02-210 Lab06 - PatternA
//October 10, 2018

//The purpose of this lab is to teach us nested loops and patterns that will help us understand how to set up nested loops 
//Ask the user for an integer between 1 - 10 : This will be the length of the pyramid --> number of rows
//Check that the inputs are between 1 and 10, and if the user does not provide an integer or if the user provides an integer that is out of range,
//indicate an error and ask again. Do so infinitely until the user provides correct input.

//The outer loop determines how many lines will be printed. 
//Since we asked the user for the size of the pyramid, we should use that input in the loop condition. 
//The inner loop determines what will be printed on each line. 
//Looking at the pattern of numbers, the inner loop is going to print out numbers until it reaches the value of the outer loop.
//So, for example when the outer loop is 1, the inner loop will print out all of the numbers until it reaches the outer loop value 
//When the outer loop increments to 2, the inner loop will print 1 2. 
//Consider using the numRows variable within the inner loop to determine how many numbers and which numbers should print.
 

import java.util.Scanner; //how you start if you need to use a scanner

public class PatternA{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
<<<<<<< HEAD
    int length=1; //number of rows
=======
    int length; //number of rows
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
    int i; //increment variable
    
    System.out.print("Please enter an integer between 1-10: "); //prompts the user for the integer between 1 and 10
    do{
    while(!myScanner.hasNextInt()){ //checks if it between 1 and 10
      System.out.println("Did not enter an integer, please try again"); //tells you did not enter an integer between 1-10
      myScanner.nextLine(); //clears the incorrect value 
    }
<<<<<<< HEAD
    if (length < 1 || length > 10)
            System.out.println("Did not enter an integer between 1 - 10, please try again");
=======
    System.out.println("Did not enter an integer between 1 - 10, please try again");
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
    length = myScanner.nextInt();
    
    }while(length < 1 || length > 10); //keeps checking until its between 1 and 10
      
<<<<<<< HEAD
      for(int rows = 1; rows <= length; rows++){ //this is for the number of rows in the program which will be the same as the input the user puts in
       
      for (i = 1; i<=rows; i++){ //then on each row you need to do columns and since this just wants to prints increasing numbers with increasing rows you incriment by i++
        System.out.print(i); //just need to print i but without the ln so the numbers prints next to each other
      }
        System.out.println(); //then outside each rows loop you do a new line but still inside the outter loop statement because you need to do it after each row 
=======
      for(int numRows = 1; numRows < length; numRows++){
       
      for (i = 1; i<=numRows; i++){
        System.out.print(i);
      }
        System.out.println();
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
      }
    
    
    
      
    }
    
    
    
  }

