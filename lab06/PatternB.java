//rebecca grady 
//CSE 02-210 Lab06 - PatternB
//October 10, 2018

//The purpose of this lab is to teach us nested loops and patterns that will help us understand how to set up nested loops 
//Ask the user for an integer between 1 - 10 : This will be the length of the pyramid --> number of rows
//Check that the inputs are between 1 and 10, and if the user does not provide an integer or if the user provides an integer that is out of
//indicate an error and ask again. Do so infinitely until the user provides correct input.

//The outer loop determines how many lines will be printed. 
//Since we asked the user for the size of the pyramid, we should use that input in the loop condition. 
//The inner loop determines what will be printed on each line. 
//Looking at the pattern of numbers, the inner loop is going to print out numbers until it reaches the value of the outer loop.
//So, for example when the outer loop is 1, the inner loop will print out all of the numbers until it reaches the outer loop value 
//When the outer loop increments to 2, the inner loop will print 1 2. 
//Consider using the numRows variable within the inner loop to determine how many numbers and which numbers should print.
 

import java.util.Scanner; //how you start if you need to use a scanner

public class PatternB{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
<<<<<<< HEAD
    int length=1; //number of rows
=======
    int length; //number of rows
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
    int i; //increment variable
    
    System.out.print("Please enter an integer between 1-10: "); //prompts the user for the integer between 1 and 10
    do{
    while(!myScanner.hasNextInt()){ //checks if it between 1 and 10
      System.out.println("Did not enter an integer, please try again"); //tells you did not enter an integer between 1-10
      myScanner.nextLine(); //clears the incorrect value 
    }
<<<<<<< HEAD
    if (length < 1 || length > 10)
            System.out.println("Did not enter an integer between 1 - 10, please try again");
=======
    //System.out.println("Did not enter an integer between 1 - 10, please try again");
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
    length = myScanner.nextInt();
    
    }while(length < 1 || length > 10); //keeps checking until its between 1 and 10
      
<<<<<<< HEAD
      for(int row = 1; row <=length; row++){ //this is for the number of rows which is the same as the user input
       
      for (i = 1; i<=length-(row-1); i++){ //then each row is printing the same number of columns that is equal to the use length, with trial and error i determined i needed to do length-(row-1), then you needed to increment i  
        System.out.print(i); //then you print i
      }
        System.out.println(); //then outside each rows loop you do a new line but still inside the outter loop statement because you need to do it after each row
=======
      for(int numRows = length; numRows >=1 ; numRows--){
       
      for (i = 1; i<=length; i++){
        System.out.print(i);
      }
        System.out.println();
>>>>>>> 0900fa04491dd4790735eb824f6bdd98e34d3f67
      }
    
    
    
      
    }
    
    
    
  }

