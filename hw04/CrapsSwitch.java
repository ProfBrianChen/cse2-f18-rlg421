//rebecca grady 
// CSE 02-210 Craps Switch 
// September 24, 2018

import java.util.Scanner; //how to use a scanner in the class
import java.lang.Math; //how to import the math class in the class

public class CrapsSwitch{ 
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner( System.in ); //declare and start the scanner
    
    System.out.print("Would you like to randomly cast dice or state the two dice you want to evaluate?: respond 'randomly cast dice' or 'state the two dice'"); //as what the person whats to do
    
    String diceChoice = myScanner.nextLine(); //store the information they respond
    
    switch(diceChoice){
      case "randomly cast dice":{
        int diceOne = (int)(Math.random()*5)+1; //would give 0-4 so then add 1 and get 1-5
        int diceTwo = (int)(Math.random()*5)+1;
          String conditionOne = "" + diceOne + "," + diceTwo; //how to use both dice value at the same time
    
          switch(conditionOne){ //call on the condition made
      case "1,1": {
        System.out.println("You rolled Snake Eyes");
      break;
      }
        
      case "1,2":{
        System.out.println("You rolled Ace Deuce");
      break;
      }
        
      case "1,3":{
        System.out.println("You rolled Easy Four");
      break;
      }
        
      case "1,4":{
        System.out.println("You rolled Fever Five");
      break;
      }
        
      case "1,5":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "1,6":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,2":{
         System.out.println("You rolled Hard four ");
      break;
      }
        
      case "2,3":{
        System.out.println("You rolled Fever five");
      break;
      }
        
      case "2,4":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "2,5":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,6":{
        System.out.println("You rolled Easy Eight");
      break;
      }
       
      case "3,3":{
        System.out.println("You rolled Hard six");
      break;
      }
        
      case "3,4":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "3,5":{
        System.out.println("You rolled Easy Eight");
      break;
      }
        
      case "3,6": {
        System.out.println("You rolled Nine");
      break;
      }
        
      case "4,4":{
        System.out.println("You rolled Hard Eight");
      break;
      }
        
      case "4,5":{
        System.out.println("You rolled Nine");
        break;
      }
      case "4,6": {
        System.out.println("You rolled Easy Ten");
        break;
      }
        
      case "5,5":{
        System.out.println("You rolled Hard Ten");
        break;
      }
        
      case "5,6":{
        System.out.println("You rolled Yo-leven");
        break;
      }
      case "6,6":{
        System.out.println("You rolled Boxcars");
        break;
      }
    }  
     
    String conditionTwo = "" + diceTwo + "," + diceOne; //now need it if diceTwo is first and diceOne is second
   switch(conditionTwo){ //only need the statement when diceOne and diceTwo are the same once 
      case "1,2":{
        System.out.println("You rolled Ace Deuce");
      break;
      }
        
      case "1,3":{
        System.out.println("You rolled Easy Four");
      break;
      }
        
      case "1,4":{
        System.out.println("You rolled Fever Five");
      break;
      }
        
      case "1,5":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "1,6":{
        System.out.println("You rolled Seven out");
      break;
      } 
      case "2,3":{
        System.out.println("You rolled Fever five");
      break;
      }
        
      case "2,4":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "2,5":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,6":{
        System.out.println("You rolled Easy Eight");
      break;
      }
      case "3,4":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "3,5":{
        System.out.println("You rolled Easy Eight");
      break;
      }
        
      case "3,6": {
        System.out.println("You rolled Nine");
      break;
      }
      case "4,5":{
        System.out.println("You rolled Nine");
        break;
      }
      case "4,6": {
        System.out.println("You rolled Easy Ten");
        break;
      }
        
      case "5,6":{
        System.out.println("You rolled Yo-leven");
        break;
      }
   }
        break;
      }
      
        
      case "state the two dice":{
        System.out.print("value of dice 1: ");
        double dice_one = myScanner.nextDouble(); //get a double 
        int diceOne = (int) dice_one; //then make it an integer
        System.out.print("value of dice 2: "); 
        double dice_two = myScanner.nextDouble(); //get a doubler
        int diceTwo = (int) dice_two; //then make it an integer
        
          String conditionOne = "" + diceOne + "," + diceTwo; //how to use both dice value at the same time
    
    switch(conditionOne){ //call on the condition made
      case "1,1": {
        System.out.println("You rolled Snake Eyes");
      break;
      }
        
      case "1,2":{
        System.out.println("You rolled Ace Deuce");
      break;
      }
        
      case "1,3":{
        System.out.println("You rolled Easy Four");
      break;
      }
        
      case "1,4":{
        System.out.println("You rolled Fever Five");
      break;
      }
        
      case "1,5":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "1,6":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,2":{
         System.out.println("You rolled Hard four ");
      break;
      }
        
      case "2,3":{
        System.out.println("You rolled Fever five");
      break;
      }
        
      case "2,4":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "2,5":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,6":{
        System.out.println("You rolled Easy Eight");
      break;
      }
       
      case "3,3":{
        System.out.println("You rolled Hard six");
      break;
      }
        
      case "3,4":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "3,5":{
        System.out.println("You rolled Easy Eight");
      break;
      }
        
      case "3,6": {
        System.out.println("You rolled Nine");
      break;
      }
        
      case "4,4":{
        System.out.println("You rolled Hard Eight");
      break;
      }
        
      case "4,5":{
        System.out.println("You rolled Nine");
        break;
      }
      case "4,6": {
        System.out.println("You rolled Easy Ten");
        break;
      }
        
      case "5,5":{
        System.out.println("You rolled Hard Ten");
        break;
      }
        
      case "5,6":{
        System.out.println("You rolled Yo-leven");
        break;
      }
      case "6,6":{
        System.out.println("You rolled Boxcars");
        break;
      }
    }  
     
    String conditionTwo = "" + diceTwo + "," + diceOne; //now need it if diceTwo is first and diceOne is second
   switch(conditionTwo){ //only need the ones with the same number once  
      case "1,2":{
        System.out.println("You rolled Ace Deuce");
      break;
      }
        
      case "1,3":{
        System.out.println("You rolled Easy Four");
      break;
      }
        
      case "1,4":{
        System.out.println("You rolled Fever Five");
      break;
      }
        
      case "1,5":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "1,6":{
        System.out.println("You rolled Seven out");
      break;
      }
      case "2,3":{
        System.out.println("You rolled Fever five");
      break;
      }
        
      case "2,4":{
        System.out.println("You rolled Easy Six");
      break;
      }
        
      case "2,5":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "2,6":{
        System.out.println("You rolled Easy Eight");
      break;
      }
      case "3,4":{
        System.out.println("You rolled Seven out");
      break;
      }
        
      case "3,5":{
        System.out.println("You rolled Easy Eight");
      break;
      }
        
      case "3,6": {
        System.out.println("You rolled Nine");
      break;
      }
      case "4,5":{
        System.out.println("You rolled Nine");
        break;
      }
      case "4,6": {
        System.out.println("You rolled Easy Ten");
        break;
      }
      case "5,6":{
        System.out.println("You rolled Yo-leven");
        break;
      }
     
   }
      }
  
      }
   }
}