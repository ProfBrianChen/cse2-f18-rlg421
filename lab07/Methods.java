//rebecca grady 
//CSE 02-210 Lab07 - Methods
//October 23, 2018

//going to generate basic random sentences
//create four methods for the four sentence components

import java.util.Scanner; //how you start if you need to use a scanner
import java.util.Random;

public class Methods{ //main method required for every java program 
  public static String adjectives(){ //method for the adjectives
    String adj = ""; //for the string to get the adjective
    Random randomGenerator = new Random(); 
    int randomInt = randomGenerator.nextInt(10); //generates random integers less than 10 (0-9)
    switch (randomInt){
      case 0: adj = "pink";
        break; 
      case 1:adj = "funny";
        break; 
      case 2:adj = "happy";
        break; 
      case 3:adj = "long";
        break; 
      case 4:adj = "short";
        break; 
      case 5:adj = "blue";
        break;
      case 6:adj = "yellow";
        break; 
      case 7:adj = "lazy";
        break; 
      case 8:adj = "energetic";
        break; 
      case 9:adj = "caring";
        break; 
    }
    return adj;
  }
  public static String subject(){ //method for the non-primary nouns for subject
    String subj = ""; //string for the subject
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generates random integers less than 10 (0-9)
    switch (randomInt){
      case 0: subj = "he";
        break; 
      case 1: subj = "she";
        break;
      case 2: subj = "fox";
        break;
      case 3: subj = "dog";
        break;
      case 4: subj = "cat";
        break;
      case 5: subj = "turtle";
        break;
      case 6: subj = "shark";
        break;
      case 7: subj = "brother";
        break;
      case 8: subj = "sister";
        break;
      case 9: subj = "homework";
        break;
    }
    return subj;
  }
  public static String verbs(){ //method for the past-tense verbs
    String verb = ""; //string for the past tense verb
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generates random integers less than 10 (0-9)
    switch (randomInt){
      case 0: verb = "jumped";
        break; 
      case 1: verb = "had";
        break;
      case 2: verb = "skipped";
        break;
      case 3: verb = "walked";
        break;
      case 4: verb = "passed";
        break;
      case 5: verb = "knew";
        break;
      case 6: verb = "threw";
        break;
      case 7: verb = "spun";
        break;
      case 8: verb = "tucked";
        break;
      case 9: verb = "saw";
        break;
    }
    return verb;
  }
  public static String objects(){ //method for the non-primary nouns for object
    String obj = ""; //for the string for object
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generates random integers less than 10 (0-9)
    switch (randomInt){
      case 0: obj = "pen";
        break;
      case 1: obj = "phone";
        break;
      case 2: obj = "necklace";
        break;
      case 3: obj = "ring";
        break;
      case 4: obj = "table";
        break;
      case 5: obj = "jacket";
        break;
      case 6: obj = "book";
        break;
      case 7: obj = "sweatshirt";
        break;
      case 8: obj = "basketball";
        break;
      case 9: obj = "sock";
        break;
    }
    return obj;
  }
  
  public static String sub(){
    String sub = subject();
    return sub;
  }
  public static String thesis(String sub){ //method for the first sentence
    System.out.println("The " + adjectives() + " " + adjectives() + " "  + sub + " "  + verbs() + " the " + adjectives() + " "  + objects()); 
  }
  
  public static String supporting(String sub){ //for the supporting sentences
    System.out.println("This " + sub + " was " + adjectives() + " " + adjectives() + " to " + verbs() + " " + objects());
    System.out.println(sub + " used " + objects() + " to " + verbs() + " " + objects() + " at the " + adjectives() + " " + subject());
  } 
  
  public static String conculsion(String sub){
    System.out.println("That " + sub + " " + verbs() + " her " + objects());
  }
  
  public static void main(String[] args) {
    Random randomGenerator = new Random();
    int randomInt = randomGenerator.nextInt(10); //generates random integers less than 10 (0-9)
    thesis(sub());
    do{
    System.out.print("Would you like another sentence? (yes or no)");
    Scanner myScanner = new Scanner( System.in );
    String answer = myScanner.next();
    if (answer.equals("yes")){
        supporting(sub());
    }} while(answer.equals("yes"));
    conclusion(sub());
   
  }
  
}     
  