// rebecca grady
// CSE 02-210 Hw06
// October 23, 2018
// developing a code to form a "X" of blank spaces wtih * in the other spots 
// ask the user for an integer between 0 and 10, validate the input
// this integer will be the size of the square matrix


import java.util.Scanner; //needed when using a scanner in the main method

public class EncryptedX{ 
  //main method required for every Java program

    public static void PrintX(int size){ //this makes it easier so it can just call on this method when given a number 
      
      int row; //for the number of rows
      int col; //for the number of columns
      
      for(row=1; row<=size; row++){ //for each row
        for(col=1; col<=size; col++){ //you want to do this in each spot of the column for that row
          if ((col == row) || (col+row == size+1)){ //you see that along the diagonal there are spaces so that is when the row == col so this gives the top left to bottom right diagonal piece
            System.out.print(" "); //from doing out the math for the diagonal from bottom left to top right you see that each time the col + row == size+1 there is a space
          }
          else { //when it is not the diagonals you just want *
            System.out.print("*");
          }

    }
        System.out.println(); //for a new line
   }
    }
    
  public static void main(String[] arg) { //this is the main method which will call on the PrintX method
    int num; //this variabel needs to be different than size because it does not work when you have the same variable name in two different places
    Scanner myScanner = new Scanner( System.in ); //need in order to call on the scanner 
      do { 
      System.out.print("Enter the size of the square (1-100): "); //asks the user for an integer between 1-100
      while(!myScanner.hasNextInt()) { //while it is not an integer
         System.out.println("Was not an integer, please enter a postive integer: "); //reasks for an integer
         myScanner.next();  //clears out the incorrect value
      } 
      num = myScanner.nextInt(); //sets the size to the input
      if (num % 2 == 0){ //if you have an even matrix for the x it won't look like an x so you need to add 1 to it to make it an odd matrix
        num = num + 1; //if it is even you add 1
      }
      else { //if it is odd
        num = num; //stays the same 
      }
    }while(num<=0 || num>100);

      PrintX(num); //now you use the inputed number in the PrintX method and it executes the above code and prints it out
    
    }

}

