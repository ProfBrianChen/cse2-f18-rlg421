//hw 08
//rebecca grady
//novemeber 13, 2018

//about arrays
//write code for three methods: 
//1- shuffle(list): shuffles the elements of the list by randomizing an index number of the list 
//and swaps the element at that index with the first element, needs to be shuffled more than 50 times 
//2- getHand(list, index, numCards): returns an array that holds the number of cards specified in numCards
//cards should be taken off the end of the list of cards
//second inovation of getHand() the index value passed into it should be continued from what it was above
//3- printArray(list): takes an array of Strings and prints out each element sepeated by a space

import java.util.Scanner;
public class Shuffling{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in); 
    //suits club, heart, spade or diamond 
    String[] suitNames={"C","H","S","D"};    
    String[] rankNames={"2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q","K","A"}; 
    String[] cards = new String[52]; 
    String[] hand = new String[5]; 
    int numCards = 0; //is 0 and needs to be changed to hand.length 
    numCards = hand.length; //assings the numCards to the new value
    int again = 1; 
    int index = 51;
    for (int i=0; i<52; i++){ 
      cards[i]=rankNames[i%13]+suitNames[i/13]; 
      System.out.print(cards[i]+" "); 
    } 
    System.out.println();
    printArray(cards); 
    shuffle(cards); 
    System.out.println(); //added some println() so that it is easier to see
    printArray(cards); 
    System.out.println(); //added some println() so that it is easier to see
    while(again == 1){ 
      hand = getHand(cards,index,numCards); 
      printArray(hand);
      index = index - numCards;
      System.out.println(); //added some println() so that it is easier to see
      System.out.println("Enter a 1 if you want another hand drawn"); 
      again = scan.nextInt(); 
    }  
  } 
  
//1- shuffle(list): shuffles the elements of the list by randomizing an index number of the list 
//and swaps the element at that index with the first element, needs to be shuffled more than 50 times
  public static String [] shuffle(String [] list){
    for(int i = 0; i < list.length ; i++){ //for each element of the list
      int s = (int)Math.random()*(list.length-1); //random integer for the spot holders
      String temp = list[i]; //need a temp value
      list[i] = list[s]; 
      list[s] = temp;
    }
    return list; //returns the list value
  } 
  
//2- getHand(list, index, numCards): returns an array that holds the number of cards specified in numCards
//cards should be taken off the end of the list of cards
//second inovation of getHand() the index value passed into it should be continued from what it was above
  public static String [] getHand(String [] list, int index, int numCards){
    String[] hand = new String[numCards]; //need a new string for the hand
    for(int i = 0; i< numCards; i++){ //for the numCards that they want
      hand[i] = list[index-i]; //want it from the back of the index above so that's what you put in [ ]
    }
    return hand; //returns the hand
  }
  
//3- printArray(list): takes an array of Strings and prints out each element sepeated by a space
  public static String [] printArray(String [] list){
    for(int i = 0; i<list.length; i++){
      System.out.print(list[i] + " ");
    }
    return list;
  }
  

}
