//rebecca grady 
// CSE 02-210 Check 
// September 12, 2018
//
//use Scanner class to obtain the original cost of the check, 
//the percentage tip they wish to pay, 
//number of ways to split the check
//determine how much each person in the group needs to pay

import java.util.Scanner; //how you start if you need to use a scanner

public class Check{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in ); //how the code is able to accept input
    System.out.print("Enter the original cost of the check in from xx.xx : ");
      //use print not println so the code does not jump to the next line
      double checkCost = myScanner.nextDouble(); 
    //this defines checkCost as the number the user put into the scanner as a double which is why myScanner.nextDouble() is used
    //for an int you used myScanner.nextInt()
    
    System.out.print("Enter the percentage tip you wish to pay as a whole number (in the form xx) : ");
    //only need to state the Scanner myScanner = new Scanner (System.in) once because now it can store numbers that user type in
    double tipPercent = myScanner.nextDouble();
    tipPercent /= 100; //use this to convert the percentage in decimal form
    
    System.out.print("Enter the number of people who went out to dinner: ");
    int numPeople = myScanner.nextInt(); //use nextInt because the number of poeple will be an integer
    
    double totalCost;
    double costPerPerson;
    int dollars; //whole dollar amount of cost
    int dimes; //for storing digits to the right of the decimal point of the cost
    int pennies; //for storing digits to the right of the decimal point of the cost
    totalCost = checkCost * (1 + tipPercent);
    costPerPerson = totalCost / numPeople;
    dollars = (int)costPerPerson; //gets the whole amount, dropping decimal fraction
    dimes = (int) (costPerPerson * 10) % 10;
    pennies = (int) (costPerPerson * 100) % 10;
    
    System.out.println("Each person in the group owes $" + dollars + '.' + dimes + pennies);
     
    
    
  } //end of main method
} //end of main class