//rebecca grady 
//CSE 02-210 Lab08 - 
//November 7, 2018

//about arrays
//create an array to hold 100 integers
//use a random number generator to put numbers from 0-99 into that array
//create another array that keeps track of how many times each number is in an array 
//use the number that is occur as the index in the other array 

import java.lang.Math;
public class Arrays{
  public static void main(String [] args){
    final int SIZE_ARRAY = 100; //array size of the random numbers is 100
    int [] random = new int[SIZE_ARRAY]; //array of the random numbers
    int [] check = new int[SIZE_ARRAY]; //array to how the number of occurences 
    int counter; //to keep track of the amount
    
    System.out.println("The numbers in the array are: ");
    for(int index = 0; index < SIZE_ARRAY; index++){ //to assign each value of the random array 
      int number = (int)(Math.random()*100); //gives random numbers from 0-99
      random[index] = number;
      System.out.print(random[index] + " "); //print the values in the array
    }
    System.out.println();
    for(int index = 0; index<SIZE_ARRAY; index++){
      check[random[index]]++;}
     
for(int index = 0; index < SIZE_ARRAY; index++){
  System.out.println(index + " occurs " + check[random[index]] + " time(s)");}
  }
}
