//rebecca grady 
// CSE 02-210 Lab05 
//October 3, 2018
//Write a program that prompts the user for the following values:
  //Course number -int
  //Department name -string 
  //The number of times the course meets in a week -int
  //The time the class begins -int if you use military time
  //The instructor name -string
  //The number of students in course -int
    //for both strings want to use nextLine
    //for both strings you need to clear the buffer but for the int you do not 

import java.util.Scanner; //how you start if you need to use a scanner

public class Lab05{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner myScanner = new Scanner( System.in );
    
    int courseNum; //initializes courseNum of an integer
    String depName = "";//initializes depName of an string
    int numClassMeets; //initializes numClassMeets of an integer
    int startTime; //initializes startTime of an integer
    String instructor = ""; //initializes instructor of an string
    int studentTotal; //initializes studentTotal of an integer
    
    
    System.out.print("Enter the course number: "); //asks for the course number
    while(!myScanner.hasNextInt()){ //checks if it is not an integer
      System.out.println("Did not enter a integer, please try again"); //tells you did not ask for an integer
      myScanner.nextLine(); //clears the incorrect value
    }
    courseNum = myScanner.nextInt(); //when it is an integer the above does not happen so it sets courseNum to that integer value
    
    System.out.print("Enter the department name: ");
    myScanner.nextLine(); //clears the value
    depName = myScanner.nextLine(); //declares the variable as the input
   
    
    System.out.print("Enter the number of times the class meets per week: "); //asks user for value
    while(!myScanner.hasNextInt()){ //checks if it is not an integer
      System.out.println("Did not enter a integer, please try again"); //tells you did not ask for an integer
      myScanner.nextLine(); //clears the incorrect value
    }
    numClassMeets = myScanner.nextInt(); //if it is an integer then sets the variable to it
    myScanner.nextLine(); //clears the value
    
    
    System.out.print("Enter the time the class starts: "); //asks user for value
    while(!myScanner.hasNextInt()){ //checks if it is not an integer
      System.out.println("Did not enter a integer, please try again"); //tells you did not ask for an integer
      myScanner.nextLine(); //clears the incorrect value
    }
    startTime = myScanner.nextInt(); //if it is an integer then sets the variable to it
    
    
    System.out.print("Enter the instructor's name: "); //asks for the value
      myScanner.nextLine(); //clears the value
      depName = myScanner.nextLine(); //declares the variable as the input
    
    System.out.print("Enter the total number of students in the class: "); //asks for the value
    while(!myScanner.hasNextInt()){ //checks if it is not an integer
      System.out.println("Did not enter a integer, please try again"); //tells you did not ask for an integer
      myScanner.nextLine(); //clears the incorrect value
    }
    studentTotal = myScanner.nextInt(); //sets the variable to the correct value
    myScanner.nextLine(); //clears the value
    
    System.out.println("The course number is " + courseNum); //prints the course number
    System.out.println("The department name is " + depName); //prints the department name
    System.out.println("The number of times the class meets each week is " + numClassMeets); //prints the times the class meets per week
    System.out.println("The time the class begins is " + startTime); //prints the class start time
    System.out.println("The instructors name is " + instructor); //prints the instructors name
    System.out.println("The total number of students in the class is "+ studentTotal); //prints the total students in the class
    
  }
}
    
