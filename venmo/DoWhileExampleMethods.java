//group E
//rebecca grady
//october 23, 2018 
//doWhileMethod in class activity
//rewriting code to use different methods and call on them 

import java.util.Scanner;

public class DoWhileExampleMethods{
  
    
    public static void printMenu() {
      System.out.println("Venmo Main Menu");
      System.out.println("1. Send Money");
      System.out.println("2. Request Money");
      System.out.println("3. Check Balance");
      System.out.println("4. Quit");
    }
    
    public static int getInt(Scanner input){
      int choice;
      while(input.hasNextInt()==false){
        System.out.println("You entered an invalid value -- try again");
        input.nextLine();
      }
      choice = input.nextInt();
      input.nextLine();
      
      return choice;
    }
    
    public static double getBalance(double balance){
      double balance; 
      System.out.println("You have $" + balance +" in your account");
      return balance;
      
    }
    
    public static double sendMoney(Scanner input, double balance);{
      String friend;
      double money;
      System.out.println("Who do you want to request money from?");
      friend = input.nextLine();
      
      System.out.println("How much money do you want to send?");
      while(input.hasNextDouble()==false){
        System.out.println("You entered an invalid value -- try again");
        input.nextLine();
      }
      
      money = input.nextDouble();
      if (money <= 0){
        System.out.println("Invalid amount entered");
      }
      else if (money >= balance){
        System.out.println("You do not have sufficient funds for this transaction");
      }
      else {
        balance = balance - money;
        System.out.println("Who do you want to send the money to?");
        friend = input.NextLine();
        
        System.out.println("You have successfully sent $" + money + " to " + friend);
      }
      return balance;
            
    }
    
    public static double requestMoney(Scanner input, double balance);{
      String friend;
      double money;
      System.out.println("Who do you want to request money from?");
      friend = input.nextLine();
      
      System.out.println("How much money do you want to request?");
      while(input.hasNextDouble()==false){
        System.out.println("You entered an invalid value -- try again");
        input.nextLine();
      }
      
      money = input.nextDouble();
      
      System.out.println("You are requestiing $" + money +" from " + friend);
      
      balance = balance + money;
      System.out.println("Once confirmed, your new balance will be $" + balance);
      return balance;
      
    }
    
  public static void main(String [] args){
    Scanner input = new Scanner(System.in);
    int choice;
    double balance = 100;
    double money = 0;
    String friend;
    
    do {
      printMenu();
      choice = getInt(input);
      switch (choice){
        case 1: balance = sendMoney(input,balance);
                break;
        case 2: balance = requestMoney(input, balance);
                break;
        case 3: getBalance(balance);
                break;
        case 4: System.out.println("Goodbye");
                break;
        default: System.out.println("you entered an invalid value -- try again");
                 break;
      }
    }while(choice !=4); 
 }
    
    
    
  
}