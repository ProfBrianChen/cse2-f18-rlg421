//hw 09
//rebecca grady
//novemeber 27, 2018

//prompts user for 15 integers for students' final grades in assending order
//checks that each input is an integer - if wrong prints an error message
//checks if the input is between 0-100 - if wrong prints a different error message
//checks if the input is greater than or less than the input before if wrong prints a different error message
//print the final input array 

//prompt the user to enter a grade to search for - using binary
//say if the grade was found or not and number of iterations used

//scramble the sorted array randomly 
//print the scrambled array 
//prompt the user to enter a grade to search for - using linear
//say if the grade was found or not and number of iterations used



import java.util.Scanner;
public class CSE2Linear{ 
  public static void main(String[] args) { 
    Scanner scan = new Scanner(System.in);
    int key;
    int [] grades = new int[15];
    System.out.println("Enter 15 ascending ints for the finals grades in CSE2:");
    for (int i = 0; i < 3; i++){
      if (!scan.hasNextInt()){
        System.out.println("Did not enter an intger");
        scan.nextLine();
      }
//      else if(i>0){
//        int temp = scan.nextInt();
//        if(temp < grades[i--]){
//          System.out.println("Needs to be in ascending order");
//          scan.nextLine();
//        }
//        else{
//          grades[i]=scan.nextInt();
//          continue;
//        }  
//      }
//      else if(scan.nextInt() <= 0 && scan.nextInt() >= 100){
//        System.out.println("Not an integer between 0 and 100");
//        scan.nextLine();
//      }
      else{
        grades[i]=scan.nextInt();
        scan.nextLine();
      }
    }
    for(int i = 0; i<grades.length; i++){
      System.out.print(grades[i] + " ");
    }
    System.out.print("Enter a grade to search for: ");
    key = scan.nextInt();
    System.out.println(binarySearch(grades, key));
    System.out.println(shuffle(grades));
    System.out.println(linearSearch(grades, key));
  }
  
  
//method for linear search
  public static String linearSearch(int grades [], int key){
    String answer = "";
    for(int i = 0; i < grades.length; i++){
      if(grades[i] == key){
        answer = "was found";
      }
      else { 
        answer = "was not found";
      }
    } 
    return answer;
  }
  
//method for binary search
  public static String binarySearch(int grades [], int key) {
    int mid; 
    int low; 
    int high; 
    
    low = 0; 
    high = grades.length - 1;
    
    String answer = "";
    
    while (high >= low){
      mid = (high + low) / 2;
      if (grades[mid] < key){
        low = mid + 1;
      }
      else if (grades[mid] > key){
        high = mid - 1;
      }
      else if (grades[mid] == key){
        answer = "was found";
      }
      else {
        answer = "was not found";
      }
    }
    return answer;
  }
  
//method for scrambling
  public static int [] shuffle(int [] grades){
    for(int i = 0; i < grades.length ; i++){ //for each element of the list
      int s = (int)Math.random()*(grades.length-1); //random integer for the spot holders
      int temp = grades[i]; //need a temp value
      grades[i] = grades[s]; 
      grades[s] = temp;
    }
    return grades; //returns the list value
  } 
  
}