//rebecca grady 
// CSE 02-210 Pyramid 
// September 18, 2018

//asks the user for the dimensions of a pyramid
//gives the volume inside the pyramid 

import java.util.Scanner; //how you start if you need to use a scanner

public class Pyramid{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner pScanner = new Scanner( System.in );
    System.out.print("Enter the length of one side of the base of the pyramid: ");
    double sideLength = pScanner.nextDouble(); 
    //this is the length and width because its a square pyramid
    
    System.out.print("Enter the height of the pyramid: ");
    double height = pScanner.nextDouble();
    
    double volume = (sideLength * sideLength * height) / 3;
    //volume = (l*w*h) / 3
    
    System.out.println("The volume of the pyramid = " + volume);
  }
}