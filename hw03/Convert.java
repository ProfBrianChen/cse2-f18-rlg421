//rebecca grady 
// CSE 02-210 Convert 
// September 18, 2018

//asks the user for doubles that represent the number of acres
//of land affected by hurricane precipitation 
//and how many inches of rain were dropped on average
//convert the quanitity of rain into cublic miles


import java.util.Scanner; //how you start if you need to use a scanner

public class Convert{
  //main method required for every java program 
  public static void main(String[] args) {
    Scanner cScanner = new Scanner( System.in );
    System.out.print("Enter the affected area in acres: ");
    double acresArea = cScanner.nextDouble(); 
    //person puts in the acres that were affected (acre)
    System.out.print("Enter the average rainfall in the affected area: ");
    double rainfall = cScanner.nextDouble();
    //person puts in how many inches of rain were dropped on average (in)
    double cubicMiles = ((acresArea * rainfall * 27154.285714285714) * (9.08169 * Math.pow(10,-13)));
    //not sure how to get the right number
                         
    System.out.println("Rain in cublic miles = " + cubicMiles + " cubic miles");
                         
  }
}