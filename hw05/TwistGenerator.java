// rebecca grady
// CSE 02-210 Simple Twists
// October 9, 2018
// purpose - to get familiar with loops - we are going to print a twist
// going to ask the user for a positive integer - int length
// use a while loop to make sure the input is an integer, if it is not then ask again
// use String junk = myScanner.next(); to clear what was typed in before you type in a new value
// integer is the desired twist length
// twist is three lines long and made of slash and X characters

//what the code would look like when length = 25
// \ /\ /\ /\ /\ /\ /\ /\ /\
//  X  X  X  X  X  X  X  X  
// / \/ \/ \/ \/ \/ \/ \/ \/

import java.util.Scanner; //how to use a scanner in the class

public class TwistGenerator{ 
 public static void main(String[] args){ 
 
   
   Scanner myScanner = new Scanner( System.in ); //how to start a new scanner to use in the program
   
   int length; //initializes the length variable so it can used in the code
   int i; //used as a increment variable for the twist
   int t; //used for the number of twists
   int rem; //used to know what to print
   
    rem = length % 3;
    t = length / 3;
   
     do { 
       System.out.print("Please enter a positive integer: "); //asks the user for a positive integer
       while(!myScanner.hasNextInt()) { //while it is not an integer
          System.out.println("Was not an integer, please enter a postive integer: "); //reasks for an integer
          myScanner.next();  //clears out the incorrect value
       } 
       length = myScanner.nextInt(); //sets the length to the input
     }while(length<=0);
  

 for(i=0; i<t; i++){ //when the twist has a remainder of zero you want to do these things \ /
   System.out.print("\\ /");}
  if (rem ==1){
    System.out.print("\\"); //when the remainder is 1 you want \ - this is because the twist goes like \ /\ /  and since you take care of \ / when it goes in a set of three you need the other slashes when it is at different spots
  }
  if (rem ==2){
       System.out.print("\\"); //when the remainder is 2 you want \
       }
   System.out.println(); //for a new line
   
   for(i=0; i<t; i++){ //when the twist has a remainder of zero you want to do these things spaceXspace
       System.out.print(" X ");
     }
     if (rem ==1){
       System.out.print(" "); //when its remainder of 1 you want a space in the twist
     }
     if (rem ==2){
       System.out.print("X");//when its remainder of 2 you want a X in the twist
     }
  
   System.out.println(); //for a new line
   
   for(i=0; i<t; i++){ //for the third line
       System.out.print("/ \\"); //when the remainder is zero you want to print the entire twist section / \
     }
     if (rem ==1){
       System.out.print("/"); //when the remaineder is 1 you just want part of it /
     }
     if (rem ==2){
       System.out.print("/"); //when the remainder is 2 you just want the last part of the twist section /
     }
   System.out.println(); //for a new line

   } }
