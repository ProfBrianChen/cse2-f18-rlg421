// rebecca grady
// CSE 02-210 Hw05
// October 11, 2018
// Gives practice with while loops
// Poker: players draw sets of 5 playing cards - called hands
// different hands have different prob. of occuring
// objective: use while loops to calculate these prob.
// options: 
// 4 of a kind: 4 card with the same face value, one side card
// 3 of a kind: 3 card with the same face value, two side card
// 2-pair: 2 cards with the same face value, another 2 cards with the same face value, one side card
// 1-pair: 2 cards with the same face value, 3 side cards
// full house: both a 2-pair and a 3 of a kind

// ask the user how many times it should generate hands
// for each hand: generate 5 random cards from 1-52
// 1-13 represent the diamonds
// 14-26 represent the clubs
// 27-39 represent the hearts
// 40-52 represent the spades

public class CardGenerator{ 
  //main method required for every Java program
  public static void main(String[] arg) {
    
    int cardOne = (int)(Math.random()*52)+1; //Math.random takes every double number from 0 till the bound
    //so in the case of ()*52 it would take every number from 0-51.9999.. so when you make it an int it becomes
    //every card from 0-51 because it truncates the decimal off 
    //you then +1 outside of it so that it changes the 0-51 to 1-52
    
    int cardTwo = (int)(Math.random()*52)+1;
    int cardThree = (int)(Math.random()*52)+1;
    int cardFour = (int)(Math.random()*52)+1;
    int cardFive = (int)(Math.random()*52)+1;
    
    String suitString = ""; //string corresponding to the suit of the card
    String identityString = ""; //string corresponding to the identity of the card
    
    
    int condition = cardNum % 13;
    switch(condition) {
      case 1: {//1,14,27,40 will be the aces
        identityString = "Ace"; //all mod by 13 = 1 will be an ace
        break;
      }
        case 2: { //need case 2-10 to make the cards that are higher than 10 still the right card
        identityString = "2";
        break;
        }
        case 3: {
        identityString = "3";
        break;
        }
        case 4: {
        identityString = "4";
        break;
        }
        case 5: {
        identityString = "5";
        break;
        }
      case 6: {
        identityString = "6";
        break;
        }
        case 7: {
        identityString = "7";
        break;
        }
        case 8: {
        identityString = "8";
        break;
        }
        case 9: {
        identityString = "9";
        break;
        }
        case 10: {
        identityString = "10";
        break;
        }
        
      case 11: { //11, 24, 37, 50 will be the jacks
        identityString = "Jack";
        break;
      }
        
      case 12: { //12, 25, 38, 51 will be the queens
         identityString = "Queen";
        break;
      }
      case 0:{ //13, 26, 39, 52 will be kings 
        //this is 0 not 13 bc the remainder will be 0 for these values
        identityString = "King";
        break;
      }
      }
     
   //System.out.println(cardNum); //test line
    
    if (cardNum < 14) {  //assigns the suit depending on which chuck of cards is drawn
      suitString = "diamonds"; 
      System.out.println("You picked the " + identityString + " of " + suitString); //prints using the strings made
    }
   
    else if (cardNum > 13 && cardNum < 27) { //does the same for the clubs
      suitString = "clubs";
      System.out.println("You picked the " + identityString + " of " + suitString);
    }
   
    else if (cardNum > 26 && cardNum < 40){ //same for the hearts
      suitString = "hearts";
      System.out.println("You picked the " + identityString + " of " + suitString);
    }
   
    else { //can just use else for the spades because you are only picking cards from 1-52 and the cards 1-39 have 
      //been assigned already so the only cards left are the ones that would be spades
      suitString = "spades";
      System.out.println("You picked the " + identityString + " of " + suitString);
    }
       
  }
}