//rebecca grady 
//CSE 02-210 HW10 - Tic Tac Toe
//December 4, 2018

//write a program for the game of tic tac toe
//make a 3x3 matrix from 1-9
//player 1 is O and player 2 is X
//when the player picks a number for a spot change that number to a O or X depending on whose turn it was
//call for which player to go
//display the board each time someone goes
//check inputs to make sure they don't put in a number not avaliable
//disply who won
//display if its a draw
//must use two dimensional arrays 

import java.util.Scanner;

public class TicTacToe {
  public static void main(String[] args)
  {
    String[][] board = new String[3][3]; //creates a 3x3 two dimensional array for the board - needs to be a string bc the numbers will change to X or O
    Scanner scan = new Scanner(System.in); //scanner
    
    //add numbers to array
    int x = 1;
    for(int i = 0; i < board.length; i++)
    {
      for(int j = 0; j < board[i].length; j++)
      {
        board[i][j] = Integer.toString(x++); //make the number integers numbers into string elements 
      }
    }
    
    //print array
    printArray(board);
    
    int player = 1; //start on player one
    boolean gameOver = false; //start with gameOver being false because will run through loop until the game is over
    int[] spots = new int[] {1,2,3,4,5,6,7,8,9}; //one dim matrix to keep track of the spots - this is for when the game is over
    while(gameOver == false) //the while loop goes until gameOver is set to true
    {
      if (player == 1)
        System.out.println("Player 1, place your O on a spot:"); //calls player 1 to enter an O
      if (player == 2)
        System.out.println("Player 2, place your X on a spot:"); //calls player 2 to enter an X
      
      int response = scan.nextInt(); //gets the response from the user
      
      //to make sure the response has not been used before
      while(spots[response-1]==0){
        System.out.println("This number has already been used, try another that is on the board");
        scan.nextLine();
        response = scan.nextInt();
      }
      
      if (response == 1) //if they put in 1, change the board depending on what player inputted the number and change the spot to 0
      {
        if (player == 1) {
          board[0][0] = "O";
          spots[0] = 0;
        }
        if (player == 2) {
          board[0][0] = "X";
          spots[0] = 0;
        }
      }
      if (response == 2) { //if they put in 2, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[0][1] = "O";
          spots[1] = 0;
        }
        if (player == 2) {
          board[0][1] = "X";
          spots[1] = 0;
        }
      }
      if (response == 3) { //if they put in 3, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[0][2] = "O";
          spots[2] = 0;
        }
        if (player == 2) {
          board[0][2] = "X";
          spots[2] = 0;
        }
      }
      if (response == 4) { //if they put in 4, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[1][0] = "O";
          spots[3] = 0;
        }
        if (player == 2) {
          board[1][0] = "X";
          spots[3] = 0;
        }
      }
      if (response == 5) { //if they put in 5, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[1][1] = "O";
          spots[4] = 0;
        }
        if (player == 2) { 
          board[1][1] = "X";
          spots[4] = 0;
        }
      }
      if (response == 6) { //if they put in 6, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[1][2] = "O";
          spots[5] = 0;
        }
        if (player == 2) {
          board[1][2] = "X";
          spots[5] = 0;
        }
      }
      if (response == 7) { //if they put in 7, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[2][0] = "O";
          spots[6] = 0;
        }
        if (player == 2) {
          board[2][0] = "X";
          spots[6] = 0;
        }
      }
      if (response == 8) { //if they put in 8, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[2][1] = "O";
          spots[7] = 0;
        }
        if (player == 2) {
          board[2][1] = "X";
          spots[7] = 0;
        }
      }
      if (response == 9) { //if they put in 9, change the board depending on what player inputted the number and change the spot to 0
        if (player == 1) {
          board[2][2] = "O";
          spots[8] = 0;
        }
        if (player == 2) {
          board[2][2] = "X";
          spots[8] = 0;
        }
      }
      
      //print the array to show the board
      printArray(board);
      
      if (player == 1) //if it was player 1 make it player 2
        player = 2;
      else //if it was player 2 make it player 1
        player = 1;
      
      
      //check win conditions 
      if (board[0][0].equals(board[0][1]) && board[0][0].equals(board[0][2]))
        gameOver = true;
      if (board[1][0].equals(board[1][1]) && board[1][0].equals(board[1][2]))
        gameOver = true;
      if (board[2][0].equals(board[2][1]) && board[2][0].equals(board[2][2]))
        gameOver = true;
      if (board[0][0].equals(board[1][0]) && board[0][0].equals(board[2][0]))
        gameOver = true;
      if (board[0][1].equals(board[1][1]) && board[0][1].equals(board[2][1]))
        gameOver = true;
      if (board[0][2].equals(board[1][2]) && board[0][2].equals(board[2][2]))
        gameOver = true;
      if (board[0][0].equals(board[1][1]) && board[0][0].equals(board[2][2]))
        gameOver = true;
      if (board[0][2].equals(board[1][1]) && board[0][2].equals(board[2][0]))
        gameOver = true; 
      
      //for if its a draw then all the spots will be filled before any of the win conditiosn can be met
      if(spots[0] == 0 && spots[1] == 0 && spots[2] == 0 && spots[3] == 0 && spots[4] == 0 && spots[5] == 0 && spots[6] == 0 && spots[7] == 0 && spots[8] == 0) {
        System.out.println("It's a Draw!");
        player = -1; //this is so that nothing prints for who won since who won is outside the loop
        gameOver = true;
      }
    }
    
    if (player == 2) //if player is 2 at the end then it prints player 1 won bc it was never changed back to player 1 bc it left the loop
      System.out.println("Player 1 wins!"); 
    if (player == 1) //if player is 1 at the end then it prints player 2 won bc it was never changed back to player 2 bc it left the loop
      System.out.println("Player 2 wins!");
    
    
  }
  
  //method for printing the board since it has to happen a number of times
  public static void printArray(String[][] board)
  {
    for(int i = 0; i < board.length; i++)
    {
      for(int j = 0; j < board[i].length; j++)
      {
        System.out.print(board[i][j] + "  ");;
      }
      System.out.println();
    }
    
    
  }
  
}



