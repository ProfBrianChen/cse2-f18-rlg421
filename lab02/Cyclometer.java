//rebecca grady 
// CSE 02-210 Welcome Class 
// September 5, 2018
//
public class Cyclometer{ 
  //main method required for every Java program
  public static void main(String[] arg) {
    int secsTrip1 = 480; //number of seconds trip 1 took
    int secsTrip2 = 3220; //number of seconds trip 2 took
    int countsTrip1 = 1561; //number of counts for trip 1
    int countsTrip2 = 9037; //number of counts for trip 2 
    
    double wheelDiameter = 27.0, //diameter of the wheel in a double so it can have decimal
    PI = 3.14159, //number of PI for easier calculations
    feetPerMile = 5280, //gives the feet per miles for easier calculations
    inchesPerFoot = 12, //gives the inches in a foot for easier calculations
    secondsPerMinute = 60; //gives the number of seconds in a minute for easier calculations
    double distanceTrip1, distanceTrip2, totalDistance; //makes these variables doubles so they can have decimal and the , allows for the double to be applied to everything until the ;
    //use , to indicate that the double applies to the ones after it until a ; is placed
    
    System.out.println("Trip 1 took "+ (secsTrip1/secondsPerMinute)+ " minutes and had "+countsTrip1+" counts."); //prints the time and count for Trip 1
    System.out.println("Trip 2 took "+ (secsTrip2/secondsPerMinute) +" minutes and had "+countsTrip2+" counts."); //prints the time and count for Trip 27
    //run the calculations; store the values here: 
    //Trip 1 took 8.0 minutes and had 1561 counts
    //Trip 2 took 53.666666666666664 minutes and had 9037 counts
    
    distanceTrip1 = countsTrip1*wheelDiameter*PI; // gives the distance in inches
    //(for each count, a rotations of the wheel travles the disameter in inches times PI)
    distanceTrip1/=inchesPerFoot*feetPerMile; //gives distance in miles
    distanceTrip2=countsTrip2*wheelDiameter*PI/inchesPerFoot/feetPerMile;
    totalDistance = distanceTrip1 + distanceTrip2;
    
    //prints out the output data
    System.out.println("Trip 1 was "+distanceTrip1+" miles");
    System.out.println("Trip 2 was "+distanceTrip2+" miles");
    System.out.println("The total distance was "+totalDistance+" miles");
    
    //run the calculations; store the values here:
    //distance of Trip 1 was 2.0897820980113635 miles
    //distance of Trip 2 was 12.098245240056817 miles
    //total distance was 14.188027338068181 miles
    
  } //end of main statement
} //end of class











