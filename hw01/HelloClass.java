//rebecca grady 
// CSE 02 Hello Class 
//
public class HelloClass{ 

  public static void main(String args[]){
    //prints the dashes for above 'welcome' to terminal window
    System.out.println(" ----------- ");
    //prints the welcome portion to terminal window
    System.out.println(" | WELCOME | ");
    //prints the dashes for below 'welcome' to terminal window
    System.out.println(" ----------- ");
    //prints the triangles to make the mountain shape above my lehigh ID
    System.out.println("  ^ ^ ^ ^ ^ ^ ");
    //prints the rest of the necessary symbols to make the mountain, note for \ you need \\ or else it doesn't work
    System.out.println(" /\\/\\/\\/\\/\\/\\ ");
    //prints lehigh ID
    System.out.println("<-R--L--G--4--2--1->");
    //does the same as above just reversed, same \\ applies that actually prints \
    System.out.println(" \\/\\/\\/\\/\\/\\/ ");
    System.out.println("  v v v v v v ");
    //prints the welcome code
    System.out.println("Hi my name is Becca, I am a sophomore from Greenwich, Connecticut studying Civil Engineering and Computer Science.");
  }

}