//rebecca grady 
// CSE 02-210 Arthmetic
// I tried to call the file Arithmetic but it said that was no allowed
// due September 11, 2018
//
public class Arthmetic{ 

  public static void main(String args[]){
    
    //Number of pairs of pants
    int numPants = 3;
    //Cost per pair of pants
    double pantsPrice = 34.98;
    
    //Number of sweatshirts
    int numShirts = 2;
    //Cost per shirt 
    double shirtPrice = 24.99;
    
    //Number of belts
    int numBelts = 1;
    //cost per belt
    double beltCost = 33.99;
    
    //the tax rate
    double paSalesTax = 0.06;
    
    
    double totalCostOfPants; //total cost of pants
    totalCostOfPants = (numPants * pantsPrice);
    System.out.println("cost of pants before sales tax = " + totalCostOfPants); //prints the cost of pants bought before sales tax 
    
    double totalCostOfShirts; //total cost of sweatshirtsshirts
    totalCostOfShirts = (numShirts * shirtPrice);
    System.out.println("cost of sweatshirts before sales tax = " + totalCostOfShirts); //prints the cost of shirts bought before sales tax 
    
    double totalCostOfBelts; //total cost of belts
    totalCostOfBelts = (numBelts * beltCost);
    System.out.println("cost of belts before sales tax = " + totalCostOfBelts); ////prints the cost of belts bought before sales tax 
    
    
    double salesTaxPants; //sales tax charged buying pants
    salesTaxPants = (totalCostOfPants * paSalesTax);
    double roundSalesTaxPants = Math.round(salesTaxPants * 100.0)/100.0; //makes it the right number of decimal places
    System.out.println("sales tax on pants = " + roundSalesTaxPants); //prints the sales tax on the pants bought
    
    double salesTaxShirts; //sales tax charged buying sweatshirtsshirt
    salesTaxShirts = (totalCostOfShirts * paSalesTax);
    double roundSalesTaxShirts = Math.round(salesTaxShirts*100.0)/100.0; //makes it the right number of decimal places
    System.out.println("sales tax on sweatshirts = " + roundSalesTaxShirts); //prints the sales tax on the sweatshirts bought
    
    double salesTaxBelts; //sales tax charged buying belts
    salesTaxBelts = (totalCostOfBelts * paSalesTax);
    double roundSalesTaxBelts = Math.round(salesTaxBelts*100.0)/100.0; //makes it the right number of decimal places
    System.out.println("sales tax on belts = " + roundSalesTaxBelts); //prints the sales tax on the belts bought 
    
    
    double totalCostBeforeTax; //total cost before tax
    totalCostBeforeTax = (totalCostOfPants + totalCostOfShirts + totalCostOfBelts);
    System.out.println("total cost of everything before tax = " + totalCostBeforeTax); //prints the total cost of everything bought before sales tax
    
    double totalSalesTax; //total sales tax
    totalSalesTax = (salesTaxPants + salesTaxShirts + salesTaxBelts);
    double roundTotalSalesTax = Math.round(totalSalesTax*100.0)/100.0; //rounds the total sales tax to two decimanl places
    System.out.println("total price of sales tax = " + roundTotalSalesTax); //prints the total sales tax of all items bought
    
    double totalCost; //total paid in this transaction, including sales tax
    totalCost = (totalCostBeforeTax + totalSalesTax);
    double roundTotalCost = Math.round(totalCost*100.0)/100.0;
    System.out.println("total price including sales tax = " + roundTotalCost); //prints the total cost of everything bought including sales tax
    
  }
}
